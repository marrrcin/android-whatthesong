package net.zablo.whatthesong;

import android.os.AsyncTask;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.zablo.whatthesong.pojo.CognitiveApiDocument;
import net.zablo.whatthesong.pojo.CognitiveApiRequest;
import net.zablo.whatthesong.pojo.CognitiveApiResponse;
import net.zablo.whatthesong.pojo.CognitiveApiTaskParameters;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by mzabl on 18.05.2017.
 */

public abstract class CognitiveApiTask extends AsyncTask<CognitiveApiTaskParameters, Void, String>
implements ICognitiveApiResponse{

    @Override
    protected String doInBackground(CognitiveApiTaskParameters... params) {
        CognitiveApiTaskParameters parameters = params[0];
        try{
            CognitiveApiRequest request = new CognitiveApiRequest(
                    new CognitiveApiDocument(parameters.text)
            );

            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            String jsonToSend = gson.toJson(request);

            // request itself
            URL endpoint  = new URL(parameters.endpoint);

            HttpsURLConnection connection =
                    (HttpsURLConnection) endpoint.openConnection();
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty(parameters.tokenHeader,
                    new String(Base64.decode(parameters.token,
                            Base64.DEFAULT),"UTF-8"));
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setConnectTimeout(3500);
            connection.setReadTimeout(3500);
            OutputStream msgStream = connection.getOutputStream();
            msgStream.write(jsonToSend.getBytes());
            msgStream.flush();
            msgStream.close();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            String responseJson = response.toString();
            CognitiveApiResponse apiResponse = gson.fromJson(responseJson, CognitiveApiResponse.class);

            return apiResponse.getDocuments()[0].getDetectedLanguages()[0].getIso6391Name();

        }catch(Exception ex){
            ex.printStackTrace();
        } catch(Error err){
            err.printStackTrace();
        } catch(Throwable th){
            th.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.onLanguageDetected(s.equals("") ? null : s);

    }

    public abstract void onLanguageDetected(String language);
}
