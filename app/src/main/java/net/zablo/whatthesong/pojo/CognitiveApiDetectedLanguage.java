package net.zablo.whatthesong.pojo;

/**
 * Created by mzabl on 18.05.2017.
 */

public class CognitiveApiDetectedLanguage
{
    private String iso6391Name;

    private String name;

    private String score;

    public String getIso6391Name ()
    {
        return iso6391Name;
    }

    public void setIso6391Name (String iso6391Name)
    {
        this.iso6391Name = iso6391Name;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getScore ()
    {
        return score;
    }

    public void setScore (String score)
    {
        this.score = score;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [iso6391Name = "+iso6391Name+", name = "+name+", score = "+score+"]";
    }
}
