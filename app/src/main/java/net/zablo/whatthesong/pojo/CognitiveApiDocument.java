package net.zablo.whatthesong.pojo;

/**
 * Created by mzabl on 18.05.2017.
 */

public class CognitiveApiDocument
{
    public CognitiveApiDocument(){

    }

    public CognitiveApiDocument(String text){
        this.text = text;
        this.id = "1";
    }

    private String id;

    private String text;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", text = "+text+"]";
    }
}
