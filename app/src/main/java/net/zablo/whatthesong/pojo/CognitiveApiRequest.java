package net.zablo.whatthesong.pojo;

/**
 * Created by mzabl on 18.05.2017.
 */

public class CognitiveApiRequest {
    public CognitiveApiRequest(){

    }

    public CognitiveApiRequest(CognitiveApiDocument doc){
        this.documents = new CognitiveApiDocument[]{doc};
    }

    private CognitiveApiDocument[] documents;

    public CognitiveApiDocument[] getDocuments ()
    {
        return documents;
    }

    public void setDocuments (CognitiveApiDocument[] documents)
    {
        this.documents = documents;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [documents = "+documents+"]";
    }
}
