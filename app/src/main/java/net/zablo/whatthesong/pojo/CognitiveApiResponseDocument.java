package net.zablo.whatthesong.pojo;

/**
 * Created by mzabl on 18.05.2017.
 */

public class CognitiveApiResponseDocument {
    private String id;

    private CognitiveApiDetectedLanguage[] detectedLanguages;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public CognitiveApiDetectedLanguage[] getDetectedLanguages ()
    {
        return detectedLanguages;
    }

    public void setDetectedLanguages (CognitiveApiDetectedLanguage[] detectedLanguages)
    {
        this.detectedLanguages = detectedLanguages;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", detectedLanguages = "+detectedLanguages+"]";
    }
}
