package net.zablo.whatthesong.pojo;

/**
 * Created by mzabl on 18.05.2017.
 */

public class CognitiveApiTaskParameters {
    public String text;
    public String endpoint;
    public String token;
    public String tokenHeader;

    public CognitiveApiTaskParameters(String text, String endpoint, String token, String tokenHeader) {
        this.text = text;
        this.endpoint = endpoint;
        this.token = token;
        this.tokenHeader = tokenHeader;
    }
}
