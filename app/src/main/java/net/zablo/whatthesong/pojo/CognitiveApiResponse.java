package net.zablo.whatthesong.pojo;

/**
 * Created by mzabl on 18.05.2017.
 */

public class CognitiveApiResponse
{
    private String[] errors;

    private CognitiveApiResponseDocument[] documents;

    public String[] getErrors ()
    {
        return errors;
    }

    public void setErrors (String[] errors)
    {
        this.errors = errors;
    }

    public CognitiveApiResponseDocument[] getDocuments ()
    {
        return documents;
    }

    public void setDocuments (CognitiveApiResponseDocument[] documents)
    {
        this.documents = documents;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [errors = "+errors+", documents = "+documents+"]";
    }
}
