package net.zablo.whatthesong;

/**
 * Created by mzabl on 18.05.2017.
 */

public interface ICognitiveApiResponse {
    void onLanguageDetected(String language);
}
