package net.zablo.whatthesong;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ReadSongService extends Service implements AudioManager.OnAudioFocusChangeListener {
    protected class LastBroadcastReceivedTimestap{
        public Long ts = null;
    }

    protected BroadcastReceiver receiver;
    protected BroadcastReceiver speechBroadcastReceiver;

    protected HashMap<String, Locale> localeLookup = new HashMap<>();

    protected final String TTS_ID = "WTS_TTS_SPEAK";

    protected final LastBroadcastReceivedTimestap lastSpeechTimestamp = new LastBroadcastReceivedTimestap();

    protected TextToSpeech textToSpeech;
    protected AudioManager audioManager;

    public ReadSongService() {

    }

    int WTS_NOTIFICATION_ID = 666;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    @Override
    public void onCreate() {

        for (Locale loc :
                Locale.getAvailableLocales()) {
            this.localeLookup.put(loc.getLanguage(),loc);
        }

        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

            }
        });

        textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {

            }

            @Override
            public void onDone(String utteranceId) {
                audioManager.abandonAudioFocus(ReadSongService.this);
            }

            @Override
            public void onError(String utteranceId) {
                audioManager.abandonAudioFocus(ReadSongService.this);
            }
        });

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        IntentFilter iF = new IntentFilter();
        // stock music player
        iF.addAction("com.android.music.metachanged");

        // MIUI music player
        iF.addAction("com.miui.player.metachanged");

        // HTC music player
        iF.addAction("com.htc.music.metachanged");

        // WinAmp
        iF.addAction("com.nullsoft.winamp.metachanged");

        // MyTouch4G
        iF.addAction("com.real.IMP.metachanged");

        // Spotify
        iF.addAction("com.spotify.music.metadatachanged");

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                List<String> toRead = new ArrayList<>();
                String album = intent.getStringExtra("album");
                String artist = intent.getStringExtra("artist");
                String title = intent.getStringExtra("track");

                String notificationText = artist + " - " + title;
                Notification n = buildNotification(getText(R.string.notification_title), (artist == null || title == null) ? "" : notificationText)
                        .build();
                startForeground(WTS_NOTIFICATION_ID, n);

                if(sharedPref.getBoolean("setting_read_artist", true)){
                    toRead.add(artist);
                }

                if(sharedPref.getBoolean("setting_read_title", true)){
                    toRead.add(title);
                }

                if(sharedPref.getBoolean("setting_read_album",false)){
                    toRead.add(album);
                }

                removeNullOrEmptyStrings(toRead);
                if(toRead.size()==0){
                    return;
                }
                final String textToSpeak = TextUtils.join(". ",toRead);

                //throttle speaking to at most 1 per second
                if(lastSpeechTimestamp.ts != null){
                    if(System.currentTimeMillis() - lastSpeechTimestamp.ts < 1000){
                        return;
                    } else {
                        lastSpeechTimestamp.ts = System.currentTimeMillis();
                    }
                }

                boolean useLanguageInference = sharedPref.getBoolean("setting_use_language_inference",true);

                if(useLanguageInference){
                    Intent cognitiveApiIntent = new Intent(getApplicationContext(),CognitiveApiService.class);
                    cognitiveApiIntent.putExtra("text", textToSpeak);
                    startService(cognitiveApiIntent);
                }
                else {
                    sendBroadcast(new Intent(getString(R.string.ready_to_speak_intent_filter))
                            .putExtra("text", textToSpeak)
                            .putExtra("language",""));
                }

            }
        };
        registerReceiver(receiver, iF);

        IntentFilter speechIF = new IntentFilter(getString(R.string.ready_to_speak_intent_filter));
        speechBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String text = intent.getStringExtra("text");
                String language = intent.getStringExtra("language");
                if(language == null || language.equals("")){
                    textToSpeech.setLanguage(Locale.getDefault());
                }
                else {
                    textToSpeech.setLanguage(localeLookup.get(language));
                }
                try {
                    Thread.sleep(333);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                boolean useDuck = sharedPref.getString("setting_speech_mode","1").equals("1");
                int result = audioManager.requestAudioFocus(ReadSongService.this,
                        AudioManager.STREAM_MUSIC,
                        useDuck ? AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK : AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if(result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){
                    Bundle params = new Bundle();
                    params.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "");
                    textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, params, TTS_ID);
                }else {
                    //todo: notify unable to speak?
                }
            }
        };

        registerReceiver(speechBroadcastReceiver, speechIF);
        lastSpeechTimestamp.ts = System.currentTimeMillis() + 1000; //1 second delay
        makeMeForeground();

        super.onCreate();
    }

    protected void removeNullOrEmptyStrings(List<String> toRead) {
        List<Integer> toRemove = new ArrayList<>();
        for(int c=0;c<toRead.size();c++){
            if(toRead.get(c)==null || toRead.get(c).equals("")) toRemove.add(c);
        }

        for (Integer idxToRemove :
                toRemove) {
            toRead.remove(idxToRemove);
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);

        super.onDestroy();
    }

    private void makeMeForeground() {
        CharSequence title = getText(R.string.notification_title);
        CharSequence content = getText(R.string.notification_message);

        Notification mNotification = buildNotification(title, content)
            .build();
        startForeground(WTS_NOTIFICATION_ID, mNotification);
    }

    private Notification.Builder buildNotification(CharSequence title, CharSequence content) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Intent stopServiceIntent = new Intent(this, ReadSongService.class);
        stopServiceIntent.setAction("STOP");
        PendingIntent pendingStopServiceIntent = PendingIntent.getService(this, 0, stopServiceIntent, 0);

        Notification.Builder mBuilder;

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.bookmark_music);




        mBuilder = new Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(content)
                .setLargeIcon(icon).setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.bookmark_music)
                .addAction(new Notification.Action(R.drawable.close,"Stop reading",pendingStopServiceIntent));



        return mBuilder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String intentAction = intent.getAction();
        if(intentAction != null && intentAction.equals("STOP")){
            stopSelf();
            return START_NOT_STICKY;
        }
        return START_STICKY;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {

    }


}
