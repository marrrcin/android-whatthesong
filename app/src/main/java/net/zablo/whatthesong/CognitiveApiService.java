package net.zablo.whatthesong;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import net.zablo.whatthesong.pojo.CognitiveApiTaskParameters;

/**
 * Created by mzabl on 18.05.2017.
 */
public class CognitiveApiService extends Service
{
    protected String textToSpeak;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.textToSpeak = intent.getStringExtra("text");

        try {
            new CognitiveApiTask() {
                @Override
                public void onLanguageDetected(String language) {
                    sendBroadcast(new Intent(getString(R.string.ready_to_speak_intent_filter))
                            .putExtra("text", textToSpeak)
                            .putExtra("language",language));
                }
            }.execute(new CognitiveApiTaskParameters(
                    textToSpeak,
                    getResources().getString(R.string.endpoint),
                    getResources().getString(R.string.token),
                    getResources().getString(R.string.token_header)
            ));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }
}